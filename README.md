# Stroke E-I Homeostasis
This code goes along with the manuscript "Multiscale Effects of Excitatory-Inhibitory Homeostasis in Lesioned Cortical Networks: A Computational Study".

Authors: Francisco Páscoa dos Santos, Jakub Vohryzek, Paul F.M.J. Verschure

Corresponding author: Francisco Páscoa dos Santos, f.pascoadossantos@gmail.com


## Introduction
Stroke-related disruptions in functional connectivity (FC) often spread beyond lesioned areas and, due to the localized nature of lesions, it is unclear how the recovery of FC is orchestrated on a global scale. Given that recovery is accompanied by long-term changes in excitability, we propose excitatory-inhibitory (E-I) homeostasis as an important mechanism of recovery and aim to model its role in restoring FC, tying it with changes in excitability. To that effect, we model stroke lesions in a connectome-constrained network of Wilson-Cowan neural masses, accounting for homeostatic scaling of local inhibition. 

In this repo, I added example code to run simulations both for model fitting and to model stroke. In addition, I include a few notebooks exemplifying the most relevant steps of the analysis of connectivity, dynamics and excitability.

## Simulations

In the Simulations folder you will find 3 .py scripts:

- **WBM.py**: contains the core code for the model and necessary functions to run it. The functions are numba compiled to improve simulation runtime.

- **run_parameter_exploration.py**: this script contains a function that you can execute to run an exploration of the parameter space. This script will run one simulation for each combination of parameters and store BOLD signals, an array containing the evolution of local inhibitory weights through the course of the simulation and the final inhibitory weights to be used for initialization of lesion simulations. This function takes three arguments: `C_vec`, `rho_vec` and `md_vec`, arrays/lists containing, respectively, the global coupling values, the target firing rate values and mean delays. You can run this code from the terminal in the following way (arguments are just examples):

```
cd YOUR_FOLDER
python -c "import run_parameter_exploration; run_parameter_exploration.run_parameter_exploration([4, 5], [0.1, 0.2, 0.3], [3, 4])"
```

- **run_lesion_simulation**: this script contains a function to run a simulation of lesion in a "healthy model". This function runs a simulation initialized with an unlesioned connectome, applies a lesion by removing all the connections from the nodes specified by the user, and lets homeostatic plasticity adapt local inhibitory weights to the lesioned connectome. BOLD activity is stored in a `[3, N_nodes, Time]` array, where the first dimension represents the 3 time points where activity is recorded: in the beginning (healthy), immediately after lesion (acute post-lesion) and after local inhibitory weights reach a new steady state (chronic post-lesion). This function takes four arguments: `C`, `rho` and `md`, floats containing, respectively, the global coupling , the target firing rate and mean delay, and `lesioned_nodes`, a list with the indexes f the nodes you want to lesion. **Important:** This function will initialize the simulations with inhibitory weights derived from the parameter exploration, so make sure there is an "init_CIE..." file in the Data/init_CIE folder correspoding to the parameters you want to run the lesion simulation with. You can run this code from the terminal in the following way (arguments are just examples):

```
cd YOUR_FOLDER
python -c "import run_lesion_simulation; run_lesion_simulation.run_lesion_simulation(4.07, 0.2, 4, [22, 23, 24])"
```

## Analysis

In this folder, I added 3 notebooks containing example analysis for the three features that we evaluted in the paper: connectivity, dynamics and excitability. The notebooks are pretty much self explanatory. For now, I have left a few example lesion simulations in the 'Simulations/Data/lesions' folder, so that you can test the analysis code. They represent examples of lesions in a low degree area (Parahippocampal gyrus) and two high degree nodes (Precentral and Superior Frontal Gyrus).

