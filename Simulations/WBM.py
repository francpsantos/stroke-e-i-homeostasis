import numpy as np
from numba import njit, prange, cuda, f8

## Helper functions

@njit(nogil=True)
def numbadiff(x):
    return (x[:, 1:] - x[:, :-1])

@njit(nogil=True)
def numbamean(x):
    res = []
    for i in range(x.shape[0]):
        res.append(x[i, :].mean())
    return np.array(res)

@njit(nogil=True)
def numbastd(x):
    res = []
    for i in range(x.shape[0]):
        res.append(x[i, :].std())
    return np.array(res)


@njit(nogil=True)
def F(x, max_S, mu, sigma):
    return max_S / (1 + np.exp((-(x - mu)/sigma)))

@njit(nogil=True)
def del_input(state, n, delays, W):

    # Gives a nxn matrix where ij is the delayed input from j to i
    state_E = state[0].copy()
    del_state = np.zeros((n, n))
    d = delays.astype(np.int64)
    
    for i in range(n):
        for j in range(n):
            del_state[i, j] = state_E[j, -d[i, j]]
        
    # Multiplies connectivity matrix with the delayed state
    del_in = np.reshape(np.sum(np.multiply(W, del_state), axis = 1), (n, 1))

    return del_in    

@njit(nogil=True)
def update_CIE(C_IE, homeo_rate, rho, E, I, dt):
    # Updates IE weight according to activity of excitatory population
    dC_IE = homeo_rate * np.multiply(I, E - rho)
    new_CIE = C_IE + dt * dC_IE
    
    return new_CIE

@njit(nogil=True)
def balloon_windkessel(signals, dt):
    
    BOLD = np.zeros(np.shape(signals))
    
    k = 0.65 # [1/s] rate of signal decay 
    gamma = 0.41  # [1/s] rate of flow-dependent elimination
    tau = 0.98 # [s] hemodynamic transit time
    alpha = 0.32 # Grubbs exponent
    rho = 0.34 # resting oxygen extraction fraction
    V0 = 0.02 # resting blood volume fraction
    dt = dt / 1000 # [s]
    
    n = np.shape(signals)[0]
    
    curr_s = 1e-10 * np.ones((1, n))
    curr_f = 1e-10 * np.ones((1, n))
    curr_v = 1e-10 * np.ones((1, n))
    curr_q = 1e-10 * np.ones((1, n))
    
    for i in range(1, np.shape(signals)[1]):

        new_q = curr_q + dt * (1/tau) * (curr_f/rho * (1-np.power((1-rho), (1/curr_f))) - np.power(curr_v, (1/alpha) - 1) * curr_q)

        new_v = curr_v + dt * (1/tau) * (curr_f - np.power(curr_v, 1/alpha))

        new_f = curr_f + dt * curr_s
    
        new_s = curr_s + dt * (np.reshape(signals[:, i-1].copy(), (1, n)) - k * curr_s - gamma*(curr_f - 1))

        y = V0 * (7*rho*(1-curr_q) + 2*(1-np.divide(curr_q, curr_v)) + (2*rho-0.2)*(1 - curr_v))

        curr_q = new_q
        curr_v = new_v
        curr_f = new_f
        curr_s = new_s

        BOLD[:, i] = np.reshape(y, (n))

    return BOLD



@njit(nogil=True)
def simulate_lesion(init_CIE, t_len, W_mat, max_time, store_time, inter_time, dt, dt_save, rho, 
                    homeo_rate, speed, lesion_nodes):
    
    W = W_mat.copy()
    
    ### Defines attributes for simulation
    # Number of neuronal populations
    n = t_len.shape[0]

    # Time constants for both populations
    tau_E = 2.5 # (ms) Excitatory time constant (tuned to gamma oscillations)
    tau_I = 5 # (ms) Inhibitory time constant

    # Constants for the response function F
    mu = 1 # firing response threshold
    sigma = 0.25 # firing response variability
    max_S = 1 # maximum response value

    # Standard deviation of additive noise      
    sd_noise = 0.01

    # Couplings from excitatory to inhibitory population
    C_EE = 3.5 # E-E coupling
    C_EI = 3.75 # E-I coupling
    C_IE = init_CIE.copy() # I-E coupling

    # Connectivity matrix and delays
    if speed > 0:
        delays_raw = t_len / speed
        delays = np.floor(delays_raw / dt)
    else:
        delays = np.zeros((t_len.shape))
    max_delay = int(np.amax(delays))
    
    # External input to the excitatory population
    P = 0.31 # Defined to put the node at the transition between stable and oscillatory activity

    # Defines vectors to store activity from excitatory populations in the model.
    # Output has the shape [recording, number of nodes, time], where recording refers to pre-lesion, post-lesion acute and post-lesion chronic.
    # This is done to avoid huge arrays with mostly irrelevant data.
    output = np.zeros((3, n, int(np.floor(store_time / dt_save))))
    curr_store = 0 # Keeps track of which recording we are in
    store_count = 0 # Keeps track of timesteps within each recording

    # Initializes activity vector with random values around the target firing rate
    # This is a vector that keeps the state from the last max_delay timesteps.
    state = rho + 0.1 * np.random.rand(2, n, max_delay+1)

    # Initiates matrix to keep track of cIE as it evolves. Only stores every 10 seconds, for simplicity
    CIE_int = 10000 / dt
    CIE_vec = np.zeros((n, int(np.floor((max_time + 3*store_time + 3*inter_time) / (CIE_int*dt)))))
    stop_window = 60 ## Window to check variation of cIE. Window time is stop_window * CIE_int. e.g. stop_window = 60 || CIE_int = 10000 / dt --> 10 minutes

    # Trigger_vec tracks which nodes have reached a new steady-state after plasticity is turned on after lesion. When all values are =1 or max-time was reached, simulation is stopped.
    trigger_vec = np.zeros(W_mat.shape[0])
    for node in lesion_nodes:
        trigger_vec[node] = 1


    # Variables to track where we are in the simulation
    stop = False
    recording = False
    stabilizing = False # If the system is in a period of stabilization of inhibitory weights.
    st = 0 # Simulation timestep

    while ~stop and st <= (int(np.floor((max_time + 3*store_time + 3*inter_time) / dt)) + 1):

        #######################################################
        ############ SYSTEM NUMERICAL INTERGRATION ############
        #######################################################

        # Gets current states (E, I) for all nodes
        curr_E = np.reshape(state[0, :, -1].copy(), (n, 1))
        curr_I = np.reshape(state[1, :, -1].copy(), (n, 1))

        # Computes delayed input from other nodes        
        #W_delayed = del_input(state, n, delays, W)
        state_E = state[0].copy()
        del_state = np.zeros((n, n))
        d = delays.astype(np.int64)

        for i in range(n):
            for j in range(n):
                if W[i, j]>0:
                    del_state[i, j] = state_E[j, -(d[i, j]+1)]

        # Multiplies connectivity matrix with the delayed state
        W_delayed = np.reshape(np.sum(np.multiply(W, del_state), axis = 1), (n, 1))        
               
        # Calculates state changes               
        E_input = C_EE * curr_E - np.multiply(C_IE, curr_I) + P + W_delayed + sd_noise * np.random.randn(n, 1)
        I_input = C_EI * curr_E + sd_noise * np.random.randn(n, 1)
        
        dE = (1/tau_E) * (-curr_E + F(E_input, max_S, mu, sigma))
        dI = (1/tau_I) * (-curr_I + F(I_input, max_S, mu, sigma))
        
        # Updates firing rate and state variable arrays using Euler method    
        new_E = curr_E + dt * dE
        new_I = curr_I + dt * dI
        state[0, :, :-1] = state[0, :, 1:]
        state[0, :, -1] = np.reshape(new_E, (n))
        state[1, :, :-1] = state[1, :, 1:]
        state[1, :, -1] = np.reshape(new_I, (n))



        #######################################
        ############ LESION PROTOCOL ##########
        #######################################

        # First recording period. Pre-lesion.
        if st >= int((inter_time)/dt) and ~recording and curr_store == 0:
            recording = True
            print('Recording: Pre-lesion')
            store_count = 0

        #Lesion
        elif st >= int((inter_time + store_time)/dt) and recording and curr_store == 0:
            recording = False
            curr_store += 1

            # Applies lesion by removing all connections from given node.
            for node in lesion_nodes:
                W[node, :] = 0
                W[:, node] = 0

        #Second recording period. Post-lesion, acute.
        elif st >= int((2*inter_time + store_time)/dt) and ~recording and curr_store == 1:
            recording = True
            print('Recording: Post-lesion Acute')
            store_count = 0

        #Turns plasticity on
        elif st >= int((2*inter_time + 2*store_time)/dt) and recording and curr_store == 1:
            recording = False
            stabilizing = True            
            curr_store += 1
            print('Stabilizing...')

        # If the maximum stabilization time has been reached before cIE reaches a steady state, stop plasticity and start recording
        elif st >= int((2*inter_time + 2*store_time + max_time)/dt) and ~recording  and curr_store == 2:
            recording = True
            print('Max Time Reached')
            print('Recording: Post-lesion Chronic')
            stabilizing = False
            store_count = 0



        ###############################################
        ############ CONTROL OF PLASTICITY ############
        ###############################################

        if (st % CIE_int) == 0:

            CIE_vec[:, int(st // CIE_int)] = np.reshape(C_IE.copy(), (n))

        # Controls plasticity. Plasticity is only applied when local inhibitory weights need to change, i.e. post-lesion
        if stabilizing:

            C_IE = update_CIE(C_IE, homeo_rate, rho, curr_E, curr_I, dt)

            ## Tests stop condition. Taking dCIE in the last stop_window, if its abs(mean) is smaller than its standard error of the mean for a given node, that node is considered stabilized
            if ((st - (2*inter_time + 2*store_time)/dt) // CIE_int) > (stop_window):

                CIE_diff = numbadiff(CIE_vec[:, (int(st // CIE_int)-stop_window) : int(st // CIE_int)]) # calculates dCIE

                mean_vec = np.abs(numbamean(CIE_diff)) # computes mean of dCIE
                std_vec = numbastd(CIE_diff) # computes the std of dCIE

                # Checks where abs(mean) is larger than SEM
                th_vec = (1-(mean_vec > ((1/np.sqrt(stop_window)) * std_vec)))

                # If node reached a steady state for the first time, it adds 1 to its position in the trigger vec
                trigger_vec += th_vec
                trigger_vec[trigger_vec > 1] = 1

                ## If all nodes reached a steady state, stops plasticity and starts recording
                if np.all(trigger_vec):

                    CIE_vec = CIE_vec[:, :int(st // CIE_int)] # Truncates CIE_vec to the current time point

                    recording = True
                    stabilizing = False
                    store_count = 0
                    print(int(st - (2*inter_time + 2*store_time)/dt))
                    print('Recording: Post-lesion Chronic')


        ######################################
        ############ DATA STORING ############
        ######################################

        # If recording started, stores data
        if recording:
            if store_count < int(np.floor(store_time / dt_save)):
                if int((st % (dt_save/dt))) == 0:
                    output[curr_store, :, store_count] = np.reshape(new_E, (n))
                    store_count += 1

            elif store_count >= int(np.floor(store_time / dt_save)) and curr_store == 2:
                stop = True
                CIE_vec = CIE_vec[:, :int(st // CIE_int)]

        # Increments steps
        st += 1
    
    return output, CIE_vec



# Runs model with an algorithm to detect when local cIE weights reach a steadt state, stopping the simulation then
@njit(nogil=True)
def simulate_steady_state(t_len, W_mat, max_time, store_time, dt, dt_save, rho, homeo_rate, speed):
    
    W = W_mat.copy()
    
    ### Defines attributes for simulation
    # Number of neuronal populations
    n = t_len.shape[0]

    # Time constants for both populations
    tau_E = 2.5 # (ms) Excitatory time constant (tuned to gamma oscillations)
    tau_I = 5 # (ms) Inhibitory time constant

    # Constants for the response function F
    mu = 1 # firing response threshold
    sigma = 0.25 # firing response variability
    max_S = 1 # maximum response value

    # Standard deviation of additive noise      
    sd_noise = 0.01

    # Couplings from excitatory to inhibitory population
    C_EE = 3.5 # E-E coupling
    C_EI = 3.75 # E-I coupling
    C_IE = 2.5 * np.zeros((n, 1)) # I-E coupling

    # Connectivity matrix and delays
    if speed > 0:
        delays_raw = t_len / speed
        delays = np.floor(delays_raw / dt)
    else:
        delays = np.zeros((t_len.shape))
        
    max_delay = int(np.amax(delays))
    
    # External input to the excitatory population
    P = 0.31 # Defined to put the node at the transition between stable and oscillatory activity

    # Defines vectors to store activity from excitatory populations in the model.
    # Output has the shape [recording, number of nodes, time], where recording refers to pre-lesion, post-lesion acute and post-lesion chronic.
    # This is done to avoid huge arrays with mostly irrelevant data.
    output = np.zeros((n, int(np.floor(store_time / dt_save))))
    store_count = 0 # Keeps track of timesteps within each recording

    # Initializes activity vector with random values around the target firing rate
    # This is a vector that keeps the state from the last max_delay timesteps.
    state = rho + 0.1 * np.random.rand(2, n, max_delay+1)

    # Initiates matrix to keep track of cIE as it evolves. Only stores every 10 seconds, for simplicity
    CIE_int = 10000 / dt
    CIE_vec = np.zeros((n, int(np.floor((max_time + store_time) / (CIE_int*dt)))))
    stop_window = 60 ## Window to check variation of cIE. Window time is stop_window * CIE_int. e.g. stop_window = 60 || CIE_int = 10000 / dt --> 10 minutes

    # Trigger_vec tracks which nodes have reached a new steady-state after plasticity is turned on after lesion. When all values are =1 or max-time was reached, simulation is stopped.
    trigger_vec = np.zeros(W_mat.shape[0])

    # Variables to track where we are in the simulation
    stop = False
    recording = False
    stabilizing = True # If the system is in a period of stabilization of inhibitory weights.
    st = 0 # Simulation timestep

    while ~stop and st <= (int(np.floor((max_time + store_time) / dt)) + 1):

        #######################################################
        ############ SYSTEM NUMERICAL INTERGRATION ############
        #######################################################

        # Gets current states (E, I) for all nodes
        curr_E = np.reshape(state[0, :, -1].copy(), (n, 1))
        curr_I = np.reshape(state[1, :, -1].copy(), (n, 1))

        # Computes delayed input from other nodes        
        #W_delayed = del_input(state, n, delays, W)
        state_E = state[0].copy()
        del_state = np.zeros((n, n))
        d = delays.astype(np.int64)

        for i in range(n):
            for j in range(n):
                if W[i, j]>0:
                    del_state[i, j] = state_E[j, -(d[i, j]+1)]

        # Multiplies connectivity matrix with the delayed state
        W_delayed = np.reshape(np.sum(np.multiply(W, del_state), axis = 1), (n, 1))        
               
        # Calculates state changes               
        E_input = C_EE * curr_E - np.multiply(C_IE, curr_I) + P + W_delayed + sd_noise * np.random.randn(n, 1)
        I_input = C_EI * curr_E + sd_noise * np.random.randn(n, 1)
        
        dE = (1/tau_E) * (-curr_E + F(E_input, max_S, mu, sigma))
        dI = (1/tau_I) * (-curr_I + F(I_input, max_S, mu, sigma))
        
        # Updates firing rate and state variable arrays using Euler method    
        new_E = curr_E + dt * dE
        new_I = curr_I + dt * dI
        state[0, :, :-1] = state[0, :, 1:]
        state[0, :, -1] = np.reshape(new_E, (n))
        state[1, :, :-1] = state[1, :, 1:]
        state[1, :, -1] = np.reshape(new_I, (n))



        ###############################################
        ############ CONTROL OF PLASTICITY ############
        ###############################################

        # If the maximum stabilization time has been reached before cIE reaches a steady state, stop plasticity and start recording
        if st >= int((max_time/dt)) and ~recording:
            recording = True
            print('Max Time Reached')
            print('Recording')
            stabilizing = False

        if (st % CIE_int) == 0:

            CIE_vec[:, int(st // CIE_int)] = np.reshape(C_IE.copy(), (n))

        # Controls plasticity. Plasticity is only applied when local inhibitory weights need to change, i.e. post-lesion
        if stabilizing:

            C_IE = update_CIE(C_IE, homeo_rate, rho, curr_E, curr_I, dt)

            ## Tests stop condition. Taking dCIE in the last stop_window, if its abs(mean) is smaller than its standard error of the mean for a given node, that node is considered stabilized
            if (st // CIE_int) > (stop_window):

                CIE_diff = numbadiff(CIE_vec[:, (int(st // CIE_int)-stop_window) : int(st // CIE_int)]) # calculates dCIE

                mean_vec = np.abs(numbamean(CIE_diff)) # computes mean of dCIE
                std_vec = numbastd(CIE_diff) # computes the std of dCIE

                # Checks where abs(mean) is larger than SEM
                th_vec = (1-(mean_vec > ((1/np.sqrt(stop_window)) * std_vec)))

                # If node reached a steady state for the first time, it adds 1 to its position in the trigger vec
                trigger_vec += th_vec
                trigger_vec[trigger_vec > 1] = 1

                ## If all nodes reached a steady state, stops plasticity and starts recording
                if np.all(trigger_vec):

                    CIE_vec = CIE_vec[:, :int(st // CIE_int)] # Truncates CIE_vec to the current time point

                    recording = True
                    stabilizing = False
                    print('Recording')


        ######################################
        ############ DATA STORING ############
        ######################################

        # If recording started, stores data
        if recording:
            if store_count < int(np.floor(store_time / dt_save)):
                if int((st % (dt_save/dt))) == 0:
                    output[:, store_count] = np.reshape(new_E, (n))
                    store_count += 1

            elif store_count >= int(np.floor(store_time / dt_save)):
                stop = True
                CIE_vec = CIE_vec[:, :int(st // CIE_int)]

        # Increments steps
        st += 1
    
    return output, CIE_vec