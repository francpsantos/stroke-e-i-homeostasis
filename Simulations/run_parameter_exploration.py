import numpy as np
from numba import njit
import WBM as WBM
import scipy.signal as sig
import time as time
from pymatreader import read_mat

def parameter_exploration(C_vec, rho_vec, md_vec):

  '''
  Runs simulation of model activity across all combinations of parameters in C_vec, rho_vec
  and md_vec. This function stores a BOLD signals, an array describing the evolution of local 
  inhibitory weights along the full simulation and an array with the final cEI weights to be 
  used for initialization of lesion simulations.

  Francisco Santos 2022 - f.pascoadossantos@gmail.com

  Args:
    C_vec (array): array with global coupling scaling factors for structural connectivity
    rho_vec (float): array with target firing rates of homeostatic plasticity
    md_vec (float): array with mean conduction delays, used to calculate synaptic velocity 
  '''

  ## Loads structural connectivity data
  mat = read_mat('Data/Connectivity/SC_90aal_32HCP.mat')
  connect = mat['mat']
  connect[connect < 10] = 0 # Removes connections with less than 10 fibers
  np.fill_diagonal(connect, 0)
  connect /= np.max(connect) # Normalizes connectivity matrix so that the maximum value is 1

  t_len = mat['mat_D'] # Tract lengths

  ## If you wish to run simulations with only cortical areas, write cortical = True
  cortical = True
  if cortical:
    # Picks only the cortical nodes
    mask = np.concatenate((np.arange(36),
                           np.array([38, 39]),
                           np.arange(42, 70),
                           np.arange(78, 90)))
              
    connect = connect[:, mask][mask]
    t_len = t_len[:, mask][mask]


  # If this line is not necessary in your system, remove it. This needs to be added because, in numba, all arrays must have the same data type
  t_len = np.float64(t_len)
  connect = np.float64(connect)

  ## Initializes local inhibitory weights as 2.5 for all nodes
  init_CIE = 2.5 * np.ones((connect.shape[0], 1))

  ############################################################################################################################
  ## This step is just to run the functions one time so that numba compiles them before running the actual simulations.     ##
  ## If you already compiled the functions, this is not necessary, but everytime you change something in the functions the  ##
  ## first simulation you run afterwards will take more time. To be safe, keep this part of the code, it takes little time  ##
  ## and it makes sure that the actual simulations are run in numba compiled code (significantly faster).                   ##
  ############################################################################################################################
  w_test = connect.copy()
  test, test2 = WBM.simulate_steady_state(t_len = t_len, 
                                          W_mat = w_test, 
                                          max_time = 1000, 
                                          store_time = 1000, 
                                          dt = 1, 
                                          dt_save = 1, 
                                          rho = 0.1, 
                                          homeo_rate = (1/2.5e3), 
                                          speed = 50)

  print('Sim. test Done')
  test = WBM.balloon_windkessel(test, 1)
  print('BW test Done')


  for C in C_vec:
    for rho in rho_vec:
      for md in md_vec:

        # Calculates conduction speed using mean delay and tract lenght
        if md == 0:
          speed = -10 # If you feed this to the simulation function, it will run a simulation with no delays
        else:
          speed = np.mean(t_len[connect>0])/md # Only considers tract lengths between areas that are connected, hence t_len[connect > 0]

        # Simulation parameters
        max_time = 500*60*1000 #Simulation time, in ms
        store_time = 35*60*1000 # Store time, in ms
        dt = 0.2
        dt_save = 1

        # Homeostatic plasticity learning rate.
        homeo_rate = (1/2.5e3) # (1/ms)

        # Scales structural connectivity
        W = C*connect.copy()

        # Runs simulations
        print('#####################################')
        print('############Simulating...############')
        print('#####################################')
        print('C = {} || rho = {} || md = {}'.format(np.around(C, 2), 
                                                     np.around(rho, 2), 
                                                     md))

        start = time.time()
        output, CIE_vec = WBM.simulate_steady_state(t_len = t_len,
                                                    W_mat = W, 
                                                    max_time = max_time, 
                                                    store_time = store_time, 
                                                    dt = dt, 
                                                    dt_save = dt_save, 
                                                    rho = rho, 
                                                    homeo_rate = homeo_rate, 
                                                    speed = speed)

        signal = WBM.balloon_windkessel(output, dt_save)

        ns = signal.shape[-1]
        frs = 1/(0.72)
        fs = 1e3/dt_save
        nrs = int(fs/frs)
        bdr = int(2.5 * 60 * fs) # Time to remove at the beginning and end of signal, to avoid boundary effects from the Balloon-Windkessel model

        n_samples = int(frs * (signal.shape[-1] - 2 * bdr) / fs)
        signal_MRI = np.zeros((signal.shape[0], n_samples))

        for n in range(signal.shape[0]):
          signal_MRI[n, :] = sig.resample(signal[n, bdr:-bdr], n_samples)

        np.save(r"Data/parameter_exploration/signal_C{}_rho{}_md{}.npy".format(np.around(C, 2), 
                                                                               np.around(rho, 2),
                                                                               md), signal_MRI)

        np.save(r"Data/parameter_exploration/CIE_C{}_rho{}_md{}.npy".format(np.around(C, 2), 
                                                                            np.around(rho, 2),
                                                                            md), CIE_vec)

        np.save(r"Data/init_CIE/init_CIE_C{}_rho{}_md{}.npy".format(np.around(C, 2), 
                                                                    np.around(rho, 2),
                                                                    md), CIE_vec[:, -1][:, None])

        end = time.time()
        print('Elapsed time: {}'.format(np.around((end-start)/60, 2)))