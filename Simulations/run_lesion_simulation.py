import numpy as np
from numba import njit
import WBM as WBM
import scipy.signal as sig
import time as time
from pymatreader import read_mat

def run_lesion_simulation(C, rho, md, lesioned_nodes):

  '''
  Runs simulation of "healthy" model activity and applies a lesion by removing nodes from structural connectivity.
  Model activity is stored pre-lesion, acute post-lesion (immediately after lesion, without applying plasticity),
  and chronic post-lesion (after local inhibitory weights reach a new steady state). This function also stores an
  array describing the evolution of local inhibitory weights along the full simulation.
  Francisco Santos 2022 - f.pascoadossantos@gmail.com

  Args:
    C (float): Global coupling scaling factor for structural connectivity
    rho (float): Target firing rate of homeostatic plasticity
    md (float): Mean conduction delay, used to calculate synaptic velocity 
    lesioned_nodes (list): list with indices of nodes to be lesioned
  '''

  ## Loads structural connectivity data
  mat = read_mat('Data/Connectivity/SC_90aal_32HCP.mat')
  connect = mat['mat']
  connect[connect < 10] = 0 # Removes connections with less than 10 fibers
  np.fill_diagonal(connect, 0)
  connect /= np.max(connect) # Normalizes connectivity matrix so that the maximum value is 1

  t_len = mat['mat_D'] # Tract lengths

  ## If you wish to run simulations with only cortical areas, write cortical = True
  cortical = True
  if cortical:
      # Picks only the cortical nodes
      mask = np.concatenate((np.arange(36),
                             np.array([38, 39]),
                             np.arange(42, 70),
                             np.arange(78, 90)))
              
      connect = connect[:, mask][mask]
      t_len = t_len[:, mask][mask]

  # If this line is not necessary in your system, remove it. This needs to be added because, in numba, all arrays must have the same data type
  t_len = np.float64(t_len)
  connect = np.float64(connect)

  ## Simulation parameters
  lesioned_nodes = np.array(lesioned_nodes)

  # Calculates conduction speed using mean delay and tract lenght
  if md == 0:
      speed = -10 # If you feed this to the simulation function, it will run a simulation with no delays
  else:
      speed = np.mean(t_len[connect>0])/md # Only considers tract lengths between areas that are connected, hence t_len[connect > 0]


  # Loads initial values of local inhibitory weights.
  init_CIE = np.load("Data/init_CIE/init_CIE_C{}_rho{}_md{}.npy".format(np.around(C, 2), 
  																	    np.around(rho, 2),
  																	    md))

  ############################################################################################################################
  ## This step is just to run the functions one time so that numba compiles them before running the actual simulations.     ##
  ## If you already compiled the functions, this is not necessary, but everytime you change something in the functions the  ##
  ## first simulation you run afterwards will take more time. To be safe, keep this part of the code, it takes little time  ##
  ## and it makes sure that the actual simulations are run in numba compiled code (significantly faster).                   ##
  ############################################################################################################################
  w_test = connect.copy()
  test, test2 = WBM.simulate_lesion(init_CIE = init_CIE, 
                                    t_len = t_len, 
                                    W_mat = w_test, 
                                    max_time = 100, 
                                    store_time = 100,
                                    inter_time = 100,
                                    dt = 0.2, 
                                    dt_save = 1, 
                                    rho = rho, 
                                    homeo_rate = 0.001, 
                                    speed = speed, 
                                    lesion_nodes = np.array([0]))

  print('Sim. test Done')
  test = WBM.balloon_windkessel(test[0, ...], 1)
  print('BW test Done')

  ##################################################
  ## This is where the actual simulation happens. ##
  ##################################################

  # Simulation parameters
  max_time = 500*60*1000 # in ms. Maximum time to run stabilization after lesion, if cIE weights are not yet stabilized
  store_time = 35*60*1000 # in ms. Time to be store at each instance (pre-lesion, immediately after lesion and after inhibitory weights stabilize)
  inter_time = 10*60*1000 # in ms. Buffer time before model acitivity is recorded, to avoid transient effects. E.g. after lesion, acute post-lesion activity is only recorded after inter_time ms.
  dt = 0.2 # Integration timestep
  dt_save = 1 # Sampling frequency to be saved

  # Homeostatic plasticity learning rate.
  homeo_rate = (1/2.5e3) # (1/ms)

  # Scales structural connectivity
  W = C*connect.copy()

  # Runs simulations
  print('#####################################')
  print('############Simulating...############')
  print('#####################################')
  print('C = {} || rho = {} || md = {}'.format(np.around(C, 2), 
                                               np.around(rho, 2), 
                                               md))

  start = time.time()
  output, CIE_vec = WBM.simulate_lesion(init_CIE = init_CIE, 
                                        t_len = t_len, 
                                        W_mat = W, 
                                        max_time = max_time, 
                                        store_time = store_time,
                                        inter_time = inter_time, 
                                        dt = dt, 
                                        dt_save = dt_save, 
                                        rho = rho, 
                                        homeo_rate = homeo_rate, 
                                        speed = speed, 
                                        lesion_nodes = lesioned_nodes)


  # Applies ballon-windkessel model to simulated data
  signal0 = WBM.balloon_windkessel(output[0, ...], dt_save)
  signals = np.zeros((3, signal0.shape[0], signal0.shape[1]))
  signals[0, ...] = signal0
  signals[1, ...] = WBM.balloon_windkessel(output[1, ...], dt_save)
  signals[2, ...] = WBM.balloon_windkessel(output[2, ...], dt_save)

  end = time.time()

  print('Elapsed time: {}'.format(np.around((end-start)/60, 2)))

  # Resamples Ballon-Windkessel output to a sampling frequency of 1/0.72 Hz, similar to actual BOLD signal recordings
  ns = signals.shape[-1]
  frs = 1/(0.72)
  fs = 1e3/dt_save
  nrs = int(fs/frs)
  if ns >= (3 * int(2.5 * 60 * fs)):
      bdr = int(2.5 * 60 * fs)
  else:
      bdr = 1
  n_samples = int(frs * (signals.shape[-1] - 2 * bdr) / fs)
  BOLD_sig_rs = np.zeros((3, signals.shape[1], n_samples))

  for i in range(3):
    for n in range(signals.shape[1]):
      BOLD_sig_rs[i, n, :] = sig.resample(signals[i, n, bdr:-bdr], n_samples)


  # Saves data from simulation
  np.save(r"Data/lesions/signal_C{}_rho{}_md{}_les{}.npy".format(np.around(C, 2), 
                                                                          np.around(rho, 2),
                                                                          md,
                                                                          lesioned_nodes), BOLD_sig_rs)

  np.save(r"Data/lesions/CIE_C{}_rho{}_md{}_les{}.npy".format(np.around(C, 2), 
                                                                         np.around(rho, 2),
                                                                         md,
                                                                         lesioned_nodes), CIE_vec)
